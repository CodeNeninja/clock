startTime();

function GetBackground(firstDeg, secondDeg, firstColor, secondColor)
{
    return `linear-gradient(${firstDeg}deg, ${firstColor} 50%, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0)), linear-gradient(${secondDeg}deg, ${secondColor} 50%, #fff 50%, #fff)`;
}

function startTime() {
    const today = new Date();
    let hours = today.getHours();
    let minutes = today.getMinutes();
    let seconds = today.getSeconds();

    const myTime = document.querySelector('.time');
    const secondsRing = document.querySelector('.seconds-ring');
    const minutesRing = document.querySelector('.minutes-ring');
    const hoursRing = document.querySelector('.hours-ring');

    const clock = document.getElementsByClassName('clock-time')[0];
    clock.style.height = clock.offsetWidth + 'px';

    const white = '#fff';
    const secColor = 'pink';
    const minColor = 'purple';
    const hourColor = 'lightblue';

    if (seconds < 30)
    {
        if (seconds !== 0)
            secondsRing.style.background = GetBackground(90, 90 + (6 * seconds), white, secColor);
        else
            secondsRing.style.background = GetBackground(90, 270, secColor, secColor);
    }
    else
        secondsRing.style.background = GetBackground(-90 + ((seconds - 30) * 6), 270, secColor, secColor);

    if (minutes < 30)
    {
        if (minutes !== 0)
            minutesRing.style.background = GetBackground(90, 90 + (6 * minutes), white, minColor);
        else
            minutesRing.style.background = GetBackground(90, 270, minColor, minColor);
    }
    else
        minutesRing.style.background = GetBackground(-90 + ((minutes - 30) * 6), 270, minColor, minColor);

    if (hours < 12)
    {
        if (hours !== 0)
            hoursRing.style.background = GetBackground(90, 90 + (15 * hours), white, hourColor);
        else
            hoursRing.style.background = GetBackground(90, 270, hourColor, hourColor);
    }
    else
        hoursRing.style.background = GetBackground(-90 + ((hours - 12) * 15), 270, hourColor, hourColor);

    if (seconds < 10) seconds = `0${seconds}`;
    if (minutes < 10) minutes = `0${minutes}`;
    if (hours < 10) hours = `0${hours}`;

    myTime.innerHTML = `${hours}:${minutes}:${seconds}`;

    setTimeout(startTime, 1000);
}